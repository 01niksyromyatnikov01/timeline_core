<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 04.03.2018
 * Time: 23:06
 */
use Models\Transactions as Transactions;

if(isset($_POST['Timeline_Owner_Key']) AND isset($_POST['Timeline_Amount_Value']) AND isset($_POST['Timeline_Success_URL']) AND isset($_POST['Timeline_Fail_URL'])) {


    $owner_hash_id = htmlspecialchars($_POST['Timeline_Owner_Key']);
    $amount_value = htmlspecialchars($_POST['Timeline_Amount_Value']);
    $success_url = htmlspecialchars($_POST['Timeline_Success_URL']);
    $fail_url = htmlspecialchars($_POST['Timeline_Fail_URL']);

    $_SESSION['owner_hash_id'] = $owner_hash_id;
    $_SESSION['amount_value'] = $amount_value;
    $_SESSION['success_url'] = $success_url;
    $_SESSION['fail_url'] = $fail_url;



    if(empty($_POST['pin_code'])) {header("Location: pay-fast/auth");exit();};



}
else if(isset($_SESSION['owner_hash_id']) AND isset($_SESSION['amount_value']) AND isset($_SESSION['success_url']) AND isset($_SESSION['fail_url']) AND isset($_POST['pin_code'])) {
    if(empty($_POST['pin_code'])) {header("Location: pay-fast/auth");exit();};

    require_once MAIN_DIR.'/core/models/Transactions.php';
    require_once MAIN_DIR.'/core/func/CheckReciever.php';

    $data = array();
    $data['pincode'] = htmlspecialchars($_POST['pin_code']);
    $data['owner_id'] = htmlspecialchars($_SESSION['owner_hash_id']);

    $Transactions = new Transactions\FastPay($data);

    if($Transactions->ID == "banned") {header('Location: banned');exit();}
    $_SESSION['id'] = $Transactions->ID;

    $Sign = new Transactions\UserSign($Transactions->ID);

    $File = new models\Files\UploadFile;
    $filename = $File->Upload();
    $result = $Sign->getSignature($filename);

    if (empty($result) OR $result == '') {header('Location: pay-fast/auth');exit();}
    $_SESSION['priv1'] = $result;
    $_SESSION['pub2'] = $Transactions->PUB2;



    if($_SESSION['amount_value'] > 0) {
        $content = $common['pay_with_com'].': '.($_SESSION['amount_value']+$_SESSION['amount_value']*0.01).' <b>TCN</b><input style="display: none" value="0"  id="cash_value" disabled>';
        $_SESSION['cash'] = $_SESSION['amount_value'];
    }
    else $content = '<div style="text-align: center;"><input class="money_input" onkeypress="number_validate(event)" name=cash_value maxlength="5"  id="cash_value"><b style="font-size: 20px"> TCN</b></div>';


}

else {header("Location: ".$_SERVER['HTTP_REFERER']); exit();}




?>





<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>USER</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://use.fontawesome.com/releases/v5.0.2/css/all.css" rel="stylesheet">
    <link href="/css/dashboard.css" rel="stylesheet">
    <link href="/css/transactions.css" rel="stylesheet">


</head>

<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Timeline</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Аккаунт <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Настройки</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Помощь</a>
                </li>
                <li class="nav-item">
                    <a id="notification_bell" class="nav-link"  onclick="ShowNot()"><i class="ml-sm-4 fa fa-bell" aria-hidden="true"></i></a>

                    <div id="notification_block" style="display:none;"></div>
                </li>
            </ul>
            <!--<form class="form-inline mt-2 mt-md-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search"> -->
            <a href="logout"><button class="btn btn-outline-success my-2 my-sm-0" type="submit">Выход</button></a>
            <!-- </form>  -->
        </div>
    </nav>
</header>

<div id="container">
    <div id="info_block" class="width_550px">
        <span><?php echo $transactions['signed_succesfully'];?></span>
        <hr>
        <div>
            <? $pub2 = $_POST['pub2'];
            echo $common['sender'].':<b class="small_size"> '.$Transactions->PUB_KEY.'</b><br/>';
            echo $common['reciever'].':<b class="small_size"> '.$Transactions->PUB2.'</b><br/>';
            echo $content;
            ?>

        </div>
        <button id="button_send_conf" class="btn btn-outline-success" onclick="Send()"><? echo $common['pay'];?></button>
    </div>
</div>




<script src="/js/common.js"></script>
<script src="/js/paynet.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery.min.js"><\/script>')</script>
<script>
    function ShowAllCode(code) {
        document.getElementById('show_all_pub2_code').innerText = code;
    }
</script>

