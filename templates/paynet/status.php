<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 17.03.2018
 * Time: 23:26
 */
require_once MAIN_DIR.'/core/models/Users.php';

$json['error'] = 0;

if(isset($_SESSION['id'])) {
    $status = htmlspecialchars($_POST['status']);
    $Actions = new Actions;
    $result = $Actions->ChangePayNetStatus($status);
    if($result) $json['result'] = 1;
    else $json['error'] = 1;
}

echo json_encode($json);