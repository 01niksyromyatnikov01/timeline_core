<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 09.03.2018
 * Time: 21:01
 */

require_once MAIN_DIR.'/core/models/Users.php';


if(isset($_SESSION['id']) AND isset($_POST['hash_id']) AND isset($_POST['old_pin_hash']) AND isset($_POST['new_pin_hash'])) {
    $hash_id = htmlspecialchars($_POST['hash_id']);
    $old_pin_hash = htmlspecialchars($_POST['old_pin_hash']);
    $new_pin_hash = htmlspecialchars($_POST['new_pin_hash']);
    if($old_pin_hash != 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855' AND $new_pin_hash != 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855' AND $old_pin_hash != $new_pin_hash) {
        $res = new PayFast($hash_id, true, $new_pin_hash, $old_pin_hash);
        $result = $res->result;
        if ($result != 0) $json['result'] = 1;
        else $json['error'] = 1;
    }
    else $json['error'] = 1;
}
else $json['error'] = 1;

echo json_encode($json);