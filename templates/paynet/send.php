<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 06.03.2018
 * Time: 23:19
 */

if(!isset($_SESSION['id'])) header('Location : pay-fast/auth');
require_once MAIN_DIR.'/core/models/Transactions.php';

use Models\Transactions\Transactions as Transactions;

$json['error'] = 1;

if(isset($_SESSION['pub2']) AND isset($_SESSION['priv1']) AND isset($_SESSION['id'])) {

    if(isset($_SESSION['cash'])) $cash = (float)$_SESSION['cash'];
    else if(isset($_POST['cash'])) $cash = (float)$_POST['cash'];
    else {header("Location: pay/fast/auth");exit();}

    $Transactions = new Transactions($_SESSION['id']);
    $cash += ($cash * 0.01);
    $cash = round($cash,2);
    $priv1 = htmlspecialchars($_SESSION['priv1']);
    $pub2 = htmlspecialchars($_SESSION['pub2']);
    $bill = $Transactions->CheckBills();

    if ($cash > $bill['active']) {
        $json['error'] = 'No active money';
    }
    else if($cash+$bill['blocked_out'] > $bill['card_limit'] AND $bill['card_limit'] != 0) {
        $json['error'] = 'Exceeded dayly limit';
    }
    else {
        if($priv1 != '' AND $pub2 != '' AND $bill != '') {
            $result = $Transactions->BlockBill();
            if ($result) {
                $result = $Transactions->Create($priv1, $pub2, $cash);
                $json['result'] = 1;
                $json['error'] = 0;
                $json['succ_redirect'] = $_SESSION['success_url'];
            }
        } else {$json['error'] = 1;$json['fail_redirect'] = $_SESSION['fail_url'];}
    }
}

echo json_encode($json);
