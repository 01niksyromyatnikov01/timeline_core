<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 03.03.2018
 * Time: 21:11
 */

require_once MAIN_DIR.'/core/models/Users.php';

$json['error'] = 0;

if(isset($_SESSION['id'])) {
    $Actions = new Actions;
    $result = $Actions->AddUserToPayNet();
    if($result) $json['result'] = 1;
    else $json['error'] = 1;
}

echo json_encode($json);