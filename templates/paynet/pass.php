<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 07.03.2018
 * Time: 23:44
 */

require_once MAIN_DIR.'/core/models/Users.php';

$json['error'] = 0;

if(isset($_SESSION['id']) AND isset($_POST['hash_id']) AND isset($_POST['pin_hash'])) {
    $hash_id = htmlspecialchars($_POST['hash_id']);
    $pin_hash = htmlspecialchars($_POST['pin_hash']);
    if($pin_hash != 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855') {
        $result = new PayFast($hash_id, false, $pin_hash, '');
        if ($result) $json['result'] = 1;
        else $json['error'] = 1;
    }
    else $json['error'] = 1;
}
else $json['error'] = 1;

echo json_encode($json);