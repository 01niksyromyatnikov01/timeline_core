<?php

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>USER</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://use.fontawesome.com/releases/v5.0.2/css/all.css" rel="stylesheet">
    <link href="/css/dashboard.css" rel="stylesheet">
    <link href="/css/transactions.css" rel="stylesheet">


</head>

<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Timeline</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Аккаунт <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Настройки</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Помощь</a>
                </li>
                <li class="nav-item">
                    <a id="notification_bell" class="nav-link"  onclick="ShowNot()"><i class="ml-sm-4 fa fa-bell" aria-hidden="true"></i></a>

                    <div id="notification_block" style="display:none;"></div>
                </li>
            </ul>
            <!--<form class="form-inline mt-2 mt-md-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search"> -->
            <a href="logout"><button class="btn btn-outline-success my-2 my-sm-0" type="submit">Выход</button></a>
            <!-- </form>  -->
        </div>
    </nav>
</header>

<div id="container">
    <div id="info_block" style="width: auto!important;" class="col-sm-10 col-md-7" >
        <span><?php echo $user_tasks['header'];?></span>
        <hr>
        <div>
            <div class="hidden"><? echo $user_tasks['info_2']; ?></div>
            <? echo $user_tasks['info_1'];?>
        </div>
        <button id="button_send_conf" class="btn btn-outline-success" onclick="CreateTask()"><? echo $user_tasks['start'];?></button>
        <input type="file" style="display: none;">
    </div>
</div>




<script src="/js/common.js"></script>
<script src="/js/transactions.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery.min.js"><\/script>')</script>
<script>
    function ShowAllCode(code) {
        document.getElementById('show_all_pub2_code').innerText = code;
    }
</script>
