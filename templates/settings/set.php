<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 21.03.2018
 * Time: 22:31
 */
use  models\Settings AS UserSettings;
$json['error'] = 0;
if(isset($_SESSION['id']) AND isset($_POST['field']) AND isset($_POST['value'])) {

    $value = htmlspecialchars($_POST['value']);
    $field = htmlspecialchars($_POST['field']);


    require_once MAIN_DIR."/core/models/Settings.php";

    $Settings = new UserSettings\UserSettings();

    if($field != 'card_limit') {
        $str = $Settings->getCurrentSettings();
        $Settings->parse($str);


        $result = $Settings->setSettings($value, $field);

    }
    else if($field === 'card_limit') {
        if($value > 0)
        $result = $Settings->setCardLimit($value);
        else $result = false;
    }
    if (!$result) $json['error'] = 1;

}
else $json['error'] = 1;

echo json_encode($json);