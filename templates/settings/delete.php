<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 26.03.2018
 * Time: 22:15
 */

$json['error'] = 0;

if($_POST['field'] == "delete_button_settings") {
    require_once MAIN_DIR."/core/models/Users.php";

    $Delete = new Delete();
    $result = $Delete->End();

    unset($Delete);

    if (!$result) $json['error'] = 1;
}
else $json['error'] = 1;

echo json_encode($json);