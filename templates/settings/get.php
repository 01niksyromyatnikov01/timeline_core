<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 21.03.2018
 * Time: 22:31
 */
use  models\Settings AS UserSettings;
$json['error'] = 0;
if(isset($_SESSION['id'])) {

    require_once MAIN_DIR."/core/models/Settings.php";

    $Settings = new UserSettings\UserSettings();
    $str = $Settings->getCurrentSettings();
    $Settings->parse($str);

    $settings_array =  $Settings->createSettingsList();

    $json['settings'] = $settings_array;

    if(empty($json['settings'])) $json['error'] = 1;
}
else $json['error'] = 1;

echo json_encode($json);