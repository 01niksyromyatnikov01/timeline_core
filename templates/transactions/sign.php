<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 11.02.2018
 * Time: 22:52
 */
use Models\Transactions as Transactions;

require_once MAIN_DIR.'/core/models/Transactions.php';
require_once MAIN_DIR.'/core/func/CheckReciever.php';

$Transactions = new Transactions\UserSign($_SESSION['id']);

$File = new models\Files\UploadFile;
$filename = $File->Upload();

if($filename == '' OR $_POST['pub2'] == '') {
    $Transactions = '';
    header('Location: ../transactions');
    exit();
}
else {
    // Public accounts
    $pub2 = htmlspecialchars($_POST['pub2']);
    $res = func\CheckReciever\getIdByPub($pub2);
    echo $res;
    if(!$res) $pub2 = func\CheckReciever\getPubByName($pub2);
    // ______________________

    $result = $Transactions->getSignature($filename);
    if (empty($result) OR $result == '') {header('Location: ../transactions');exit();}
    $_SESSION['cash'] = htmlspecialchars($_POST['coins']);
    $_SESSION['priv1'] = $result;
    $_SESSION['pub2'] = $pub2;
}
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>USER</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://use.fontawesome.com/releases/v5.0.2/css/all.css" rel="stylesheet">
    <link href="/css/dashboard.css" rel="stylesheet">
    <link href="/css/transactions.css" rel="stylesheet">


</head>

<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Timeline</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Аккаунт <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Настройки</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Помощь</a>
                </li>
                <li class="nav-item">
                    <a id="notification_bell" class="nav-link"  onclick="ShowNot()"><i class="ml-sm-4 fa fa-bell" aria-hidden="true"></i></a>

                    <div id="notification_block" style="display:none;"></div>
                </li>
            </ul>
            <!--<form class="form-inline mt-2 mt-md-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search"> -->
            <a href="logout"><button class="btn btn-outline-success my-2 my-sm-0" type="submit">Выход</button></a>
            <!-- </form>  -->
        </div>
    </nav>
</header>

<div id="container">
    <div id="info_block" class="width_550px">
        <span><?php echo $transactions['signed_succesfully'];?></span>
        <hr>
        <div>
            <? $pub2 = $_POST['pub2'];
            echo $common['sender'].':<b class="small_size"> '.$Transactions->PUB_KEY.'</b><br/>';
            echo $common['reciever'].':<b class="small_size"> '.$pub2.'</b><br/>';
            echo $common['pay_with_com'].': '.($_POST['coins']+$_POST['coins']*0.01).' <b>TCN</b>';
            ?>

        </div>
        <button id="button_send_conf" class="btn btn-outline-success" onclick="Send()"><? echo $common['pay'];?></button>
        <input type="file" style="display: none;">
    </div>
</div>




<script src="/js/common.js"></script>
<script src="/js/transactions.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery.min.js"><\/script>')</script>
<script>
    function ShowAllCode(code) {
        document.getElementById('show_all_pub2_code').innerText = code;
    }
</script>
