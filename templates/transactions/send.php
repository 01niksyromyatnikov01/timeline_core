<?php
if(!isset($_SESSION['id'])) header('Location : ../../../user');
require_once MAIN_DIR.'/core/models/Transactions.php';

use Models\Transactions\Transactions as Transactions;

$json['error'] = 1;

if(isset($_SESSION['cash']) AND isset($_SESSION['pub2']) AND isset($_SESSION['priv1']) AND isset($_SESSION['id'])) {
    $Transactions = new Transactions($_SESSION['id']);
    $cash = (float)$_SESSION['cash'];
    $cash += ($cash * 0.01);
    $cash = round($cash,2);
    $priv1 = htmlspecialchars($_SESSION['priv1']);
    $pub2 = htmlspecialchars($_SESSION['pub2']);
    $bill = $Transactions->CheckBills();

    if ($cash > $bill['active']) {
        $json['error'] = 'No active money';
    }
    else if($cash+$bill['blocked_out'] > $bill['card_limit'] AND $bill['card_limit'] != 0) {
        $json['error'] = 'Exceeded dayly limit';
    }
    else {
        if($priv1 != '' AND $pub2 != '' AND $bill != '') {
        $result = $Transactions->BlockBill();
        if ($result) {
            $result = $Transactions->Create($priv1, $pub2, $cash);
                $json['result'] = 1;
                $json['error'] = 0;
        }
        } else {$json['error'] = 1;}
    }
}

echo json_encode($json);



