<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 27.12.2017
 * Time: 18:41
 */
date_default_timezone_set('Europe/Kiev');
define('MAIN_DIR', dirname(__FILE__));
session_start();

include 'localization/index.php';
include 'core/models/Settings.php';


$url = explode("/",$_SERVER["REQUEST_URI"]);

// Languages
if($url['1'] == 'EN') {
    setcookie('lang','en');
    header("Location: ".$_SERVER['HTTP_REFERER']);
}
if($url['1'] == 'RU') {
    setcookie('lang','ru');
    header("Location: ".$_SERVER['HTTP_REFERER']);
}
//  END Languages

if($url['1'] == 'api') {
    if($url['2'] == '') include 'templates/api/index.temp';
    else if($url['2'] == 'payframe') include 'templates/api/payframe.php';


}

if($url['1'] == 'index.html') {
    header('Location: index.php');
}


if($url['1'] == 'users' AND $url['2'] == 'new') {
    include 'templates/new_user.temp';
}

if($url['1'] == 'download' AND $url['2'] == 'key') {
    include 'templates/key_download.temp';
}

if($url['1'] == 'auth' AND $url['2'] == 'login') {
    include 'templates/login.temp';
}

if($url['1'] == 'auth' AND $url['2'] == 'check') {
    $file = $url['3'];
    include 'templates/login_check.temp';
}

if($url['1'] == 'auth' AND $url['2'] == 'connecting') {

    if(isset($_SESSION['id']) AND isset($_SESSION['status']) AND isset($_SESSION['ip'])) {
        include 'templates/create_conn.temp';
    }
    //else {header('Location:index.html');}
}


if($url['1'] == 'upload' AND $url['2'] == 'key') {

    include 'templates/upload_file.temp';
}

if($url['1'] == 'user') {
    if(isset($_SESSION['id']) AND isset($_SESSION['ip'])) {

        if (preg_match('#^[0-9]+$#', $url['2']) AND $url['2'] == $_SESSION['id']) {
            include 'templates/user.temp';
        }


        if ($url['2'] == 'cluster' AND !isset($url['3'])) {
            include 'templates/cluster.temp';
        }

        if ($url['2'] == 'cluster' AND $url['3'] == 'send-invite') {
            if (isset($url['4'])) {
                $rec_id = $url['4'];
            }
            include 'templates/cluster/send_invite.temp';
        }

        if ($url['2'] == 'check') {
            if ($url['3'] == 'notifications') {
                include 'templates/check/notifications.temp';
            }
        }
        if ($url['2'] == 'delete') {
            if ($url['3'] == 'notifications') {
                include 'templates/delete/notifications.temp';
            }
        }
        if ($url['2'] == 'join') {
            if ($url['3'] == 'cluster') {
                include 'templates/join/cluster.temp';
            }
        }

        // Settings
        if($url['2'] == 'settings') {
            if($url['3'] == '')
            include 'templates/settings/index.temp';
            else if($url['3'] == 'get')
            include 'templates/settings/get.php';
            else if($url['3'] == 'set')
            include 'templates/settings/set.php';
            else if($url['3'] == 'delete')
            include 'templates/settings/delete.php';
        }

        //

        // Mining

        if ($url['2'] == 'work') {
            include 'templates/mining.temp';
        }


        //


        // Tasks
        if($url['2'] == 'tasks') {
            include 'templates/tasks/index.php';
        }

        //

        // Transactions
        if($url['2'] == 'transactions') {
            if ($url['2'] == 'transactions' AND $url['3'] == '') {
                include 'templates/transactions/index.temp';
            }
            $Settings = new \models\Settings\UserSettings();
            if (!$Settings->getConfig("off_transactions")) {
                if ($url['2'] == 'transactions' AND $url['3'] == 'send') {
                    include 'templates/transactions/send.php';
                }
                if ($url['2'] == 'transactions' AND $url['3'] == 'verify') {
                    include 'templates/transactions/verify.temp';
                }
                if ($url['2'] == 'transactions' AND $url['3'] == 'sign') {
                    include 'templates/transactions/sign.php';
                }
                if ($url['2'] == 'transactions' AND $url['3'] == 'check_reciever') {
                    include 'templates/transactions/check_reciever.php';
                }
            } else echo json_encode(['error' => 2]);
        }


        // Internet Payment
        if ($url['2'] == 'paynet') {
            $Settings = new \models\Settings\UserSettings();
            if(!$Settings->getConfig("off_transactions")) {
                if ($url['3'] == '') include 'templates/paynet/index.temp';
                if ($url['3'] == 'join') include "templates/paynet/connect.php";
                if ($url['3'] == 'status') include "templates/paynet/status.php";
                if ($url['3'] == 'pass') {
                    if ($url['4'] == 'change') include "templates/paynet/change_pass.php";
                    else include "templates/paynet/pass.php";
                }
            }
            else include "templates/warnings/no-permissions.temp";
        }


        if ($url['2'] == 'setCoords') {
            include 'templates/set_coords.temp';
        }

        if ($url['2'] == 'logout') {
            include 'templates/logout.temp';
        }
    }
    else include 'templates/login.temp';
}

if($url['1'] == 'pay-fast') {
    if($url['2'] == 'auth') include 'templates/paynet/auth.temp';
    if($url['2'] == 'send') include 'templates/paynet/send.php';
    if($url['2'] == 'banned') include 'templates/paynet/payfast.php';
    if($url['2'] == '') include 'templates/paynet/payfast.php';
}


if($url['1'] == 'warning') {
    $type = $url['2'];
    include 'templates/warnings/index.php';
}




else if($url['1'] == 'banned') {
    include 'templates/banned.temp';
}
else if($url['1'] == 'user' AND $url['2'] == '') {header('Location: ../index.html');}



