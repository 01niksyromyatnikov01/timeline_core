<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 28.12.2017
 * Time: 19:56
 */

namespace models\Files;

abstract class File {
    protected $fp,$text,$filename,$filedir,$tmp;
}

class CreateFile extends File {
    public $config;
    function __construct($config) {
        $this->config = $config;
        if(!empty($this->config)) $this->Create();
    }

    private function Create() {

        $this->fp = fopen($this->config['filename'],'w+');
    }
    function Write() {
        if(isset($this->fp)) {
            $write = fwrite($this->fp, $this->config['text']);
            return $write;
        }
        else {throw new \Exception("Error in file stream");}
    }


    function Close() {
        if(isset($this->fp)) {
            return $result = fclose($this->fp);
        }
    }

    function Delete()
    {
        unset($this->fp);
        return True;
    }
}

class UploadFile {

   function Upload() {
       $dir = MAIN_DIR.'/upload/';
       $file = $dir . basename($_FILES['uploadfile']['name']);

       if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file)) {
           //echo "Файл корректен и был успешно загружен.";
       } else {
          // echo "Возможная атака с помощью файловой загрузки!";
       }
       return $_FILES['uploadfile']['name'];
   }
}


class ReadFile {
    function ReadFile($filename) {
        $file['hash'] = file_get_contents(MAIN_DIR.'/upload/'.$filename, NULL, NULL, 0, 105);
        $file['ip'] =  file_get_contents(MAIN_DIR.'/upload/'.$filename, NULL, NULL, 105, 20);
        return $file;
    }
}