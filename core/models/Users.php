<?php
//namespace models\Users;

use function Hash\GenHash;
require_once MAIN_DIR."/core/func/Hash.php";


abstract  class Autoload {
    function __autoload($class_name) {
        require_once MAIN_DIR."/core/models/".$class_name.".php";
    }
}

abstract  class Users {
    protected $hash,$id,$bd,$length;

    function initHashLength($length) {
        $this->length = $length;
    }
    function __autoload($class_name) {
        require_once MAIN_DIR."/core/models/".$class_name.".php";
    }

}


class CreateUsers extends Users {
    protected $ip,$DB,$File;
    public $res=1,$config;

    function __construct() {
        parent::__autoload("Database");
        require_once MAIN_DIR."/core/func/IP.php";
        $this->ip = getRealIP();
        $this->DB = new models\Database\Database();
        $query = array("table" => "connects" , "wts" => "`id`" , "where" => "`ip` = '$this->ip' ");
        $result = $this->DB->SELECT($query);
        $res_row = $this->DB->pushToArray($result);
        if(!isset($res_row['id']))   $this->Create();
         else $this->res = 0;
    }


     function Create() {
        parent::initHashLength(100);
        $hash = GenHash($this->length);
        // move query data to the array

        $hash_md = md5($hash);
        $query = array("table" => "connects" , "cells" => "(`hash_key`,`ip`)" , "values" => "('$hash_md','$this->ip')");
        parent::__autoload("Files");

        $text = $hash.$this->ip;
        $this->config = array("hash" => "$hash" ,"filename" => MAIN_DIR.'/download/'.$hash.'.keyTL', "text" => "$text");
        $this->File = new models\Files\CreateFile($this->config);
        if($this->File->Write()) {
            $result = $this->DB->INSERT($query);
            $id = $this->DB->getLastId();
            $array = $this->AddPubKey($id);
            $pub_key = hash('sha256',substr($hash,$array['start'],$array['finish']));
            $query = array("table" => "connects" , "values" => "`pub_key` = '$pub_key' ","where"=>"`id`=$array[id]");
            $this->DB->UPDATE($query);
            $this->CreateBill($pub_key);
            $this->CreateSettings($id);
        }
        else {throw new Exception("Error while creating file");}
        $this->res = $this->File->Close();
        $this->res = $this->File->Delete();

    }

    function CreateBill($pub_key) {
        $query = array('table' => 'bills', 'cells'=>'(`pub_key`)', 'values' => "('$pub_key')");
        $result = $this->DB->INSERT($query);
    }

    function CreateSettings($id) {
        $query = array('table' => 'settings', 'cells'=>'(`u_id`)', 'values' => "('$id')");
        $result = $this->DB->INSERT($query);
    }


    function AddPubKey($id) {
            $array = [];
            if($id%2 != 0) {
                $array['start'] =$id%2 + 5;
                $array['finish'] = 32;
            }
            else {
                $array['start'] = 5;
                $array['finish'] = 32;
            }
            $array['id'] = $id;
            return $array;
        }

}



class Auth extends Users {
    private $user,$DB,$Settings;
    function __construct() {
        parent::__autoload("Files");
        parent::__autoload("Database");
        parent::__autoload("Settings");
    }
    function LogIn($filename) {
        $File = new models\Files\ReadFile;
        $this->user = $File->ReadFile($filename);
        $this->DB = new models\Database\Database();
        $this->Settings = new models\Settings\UserSettings();


        $query = array("table" => "`connects`,`settings`" , "wts" => "`ip`,connects.`id`,settings.`value`" , "where" => "`hash_key` ='". md5($this->user['hash']) ."' AND settings.`u_id` = connects.`id`");
        $result = $this->DB->SELECT($query);
        $res_row = $this->DB->pushToArray($result);

        if($this->Settings->getConfig('one_ip_safety')) {
            if ($res_row['ip'] != $this->user['ip']) {
                return 2;
            }
        }

        if(isset($res_row['ip'])) {$_SESSION['status'] = 'logged';$_SESSION['ip'] = $this->user['ip'];$_SESSION['id'] = $res_row['id'];setcookie('u_id',$_SESSION['id']); return 1;}
        else return 0;
    }

}


class Delete extends Users {
    private $DB;
    private $head_key = "Timeline";
    function __construct()
    {
        parent::__autoload("Database");
        $this->id  = htmlspecialchars($_SESSION['id']);
        $this->DB = new models\Database\Database();
    }


    function End() {
        $query = array("table" => "bills", "wts" => '`active`,`pub_key`', "where" => "`pub_key` = (SELECT `pub_key` FROM `connects` WHERE `id` = $this->id)");
        $res = $this->DB->SELECT($query);
        $result = $this->DB->pushToArray($res);
        $money = $result['active'];
        $pub_key = $result['pub_key'];
        $query = array("table" => "connects", "where" => "`id` = $this->id");
        $result = $this->DB->DELETE($query);
        if($result) {$query = array("table" => "bills", "where" => "`pub_key` = '$pub_key'");
        $result = $this->DB->DELETE($query);
        if($result) {
            $query = array("table" => '`bills`', "values" => "`active` = `active`+'$money' ", "where" => "`pub_key` = '$this->head_key'");
            $result = $this->DB->UPDATE($query);
            return $result;
        }
        else return false;
        }
        else return false;
    }


    function __destruct()
    {
        $_SESSION['id'] = '';
        $_SESSION['ip'] = '';
        session_destroy();
    }
}




class Actions extends Users {
    private $u_id,$DB;
    function __construct()
    {
        parent::__autoload("Database");
        $this->u_id = htmlspecialchars($_SESSION['id']);
        $this->DB = new models\Database\Database();
    }

    function UpdateAction($act) {
        $query = array("table" => "user_actions" , "cells" => "(`u_id`,`trans_id`,`act`,`coins`)" , "values" => "('$this->u_id','$act[trans_id]','$act[type]',$act[coins])");
        $this->DB->INSERT($query);
    }

    function CheckUserInPayNet() {
        $query = array("table" => "pay_campaigns" , "wts"=>"`id`,`hash_id`,`pin`", "where" => "`bill_id` = (SELECT bills.`id` FROM `bills` WHERE bills.`pub_key` = (SELECT `pub_key` FROM `connects` WHERE connects.`id` = $this->u_id)) ");
        $result = $this->DB->SELECT($query);
        $res = $this->DB->pushTOArray($result);
        if(empty($res['pin']) AND empty($res['hash_id'])) {$res['answer'] = false; $res['pin'] = true;}
        else if(empty($res['pin'])) {$res['answer'] = false; $res['pin'] = false;}
        else {$res['answer'] = true;}
        return $res;
    }


    function Blablabla() {
        $query = array("table" => "pay_campaigns" , "wts"=>"`id`", "where" => "`bill_id` = (SELECT bills.`id` FROM `bills` WHERE bills.`pub_key` = (SELECT `pub_key` FROM `connects` WHERE connects.`id` = $this->u_id)) ");
        $result = $this->DB->SELECT($query);
        $res = $this->DB->pushTOArray($result);
        return $res['id'];
    }

    function AddUserToPayNet() {
        $id = rand(0,999999);
        $id .= $id;
        $hash_id = hash('sha256',$id);
        $hash_id = substr($hash_id,0,16);
        $query = array("table" => 'pay_campaigns', "cells" => "(`bill_id`,`hash_id`)", "values" => "((SELECT bills.`id` FROM `bills` WHERE bills.`pub_key` = (SELECT `pub_key` FROM `connects` WHERE connects.`id` = $this->u_id)),'$hash_id')");
        $result = $this->DB->INSERT($query);
        return $result;
    }


   public function ChangePayNetStatus($status = 0) {
        if($status == 0) $status = 'blocked';
        else $status = 'active';

        $query = array("table" => 'pay_campaigns', 'values' => "`status` = '$status'", 'where' => "`bill_id` = (SELECT bills.`id` FROM `bills` WHERE bills.`pub_key` = (SELECT `pub_key` FROM `connects` WHERE connects.`id` = $this->u_id))");
        return $this->DB->UPDATE($query);
    }

    public function CheckPayNetStatus() {

        $query = array("table" => 'pay_campaigns', 'wts' => "`status`", 'where' => "`bill_id` = (SELECT bills.`id` FROM `bills` WHERE bills.`pub_key` = (SELECT `pub_key` FROM `connects` WHERE connects.`id` = $this->u_id))");
        $result = $this->DB->SELECT($query);
        $res = $this->DB->pushToArray($result);
        if($res['status'] == 'active') return 1;
        else return 0;
    }

}

class PayFast extends Autoload {
    private $hash_id,$pass_hash,$old_pass_hash,$DB;
    public $result;

    function __construct($hash_id,$pass = true,$pass_hash = '',$old_pass_hash)
    {
        parent::__autoload("Database");
        $this->DB = new models\Database\Database();

        $this->hash_id = $hash_id;
        $this->pass_hash = $pass_hash;
        $this->old_pass_hash = $old_pass_hash;
        if(!$pass) return $this->result = $this->setPass();
        else if($pass) return $this->result =  $this->changePass();
        else return false;
    }

    private function setPass() {
        $query = array("table" => 'pay_campaigns', "values" => "`pin` = '$this->pass_hash', `status` = 'active'", "where" => "`hash_id` = '$this->hash_id'");
        $result = $this->DB->UPDATE($query);
        return $result;
    }

    private function changePass() {
        $query = array("table" => 'pay_campaigns', "values" => "`pin` = '$this->pass_hash'", "where" => "`hash_id` = '$this->hash_id' AND `pin` = '$this->old_pass_hash'");
        $result = $this->DB->UPDATE($query);
        return $this->DB->getLastAffectedId();
    }



}












