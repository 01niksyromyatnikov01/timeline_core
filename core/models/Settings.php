<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 19.03.2018
 * Time: 23:07
 */

namespace models\Settings;

abstract class Settings
{
   protected $setting_keys;

   abstract function getCurrentSettings();
   abstract function setSettings($value,$field);
   abstract function uploadSettings($settings);

   function parse($str) {
       return preg_split('//', $str, -1, PREG_SPLIT_NO_EMPTY);
   }

    function __autoload($class_name) {
        require_once MAIN_DIR.'/core/models/'.$class_name.".php";
    }

}

class UserSettings extends Settings {
    private $DB,$U_ID,$settings;
    public $ready_settings;


    function __construct($id = 0)
    {
      parent::__autoload("Database");
      $this->DB = new \models\Database\Database();
      if($id == 0) {
          $this->U_ID = htmlspecialchars($_SESSION['id']);
      }
      else $this->U_ID = $id;

        $this->setting_keys = array("type_acc","status_acc","one_ip_safety","secret_link_safety","invites_cluster","off_transactions");
    }

    function setSettings($value,$field) {
        $index = array_search($field,$this->setting_keys);
        $this->settings[$index] = $value;
        return $this->uploadSettings($this->MakeStringFromArray($this->settings));
    }


    function setCardLimit($value) {
        $value = htmlspecialchars($value);
        $query = array("table" => 'bills', "values" => "`card_limit` = '$value'", "where" => "`pub_key` = (SELECT `pub_key` FROM `connects` WHERE `id` = $this->U_ID)" );
        $result = $this->DB->UPDATE($query);
        return $result;
    }

    function MakeStringFromArray($array) {
        $string = '';
        for($i=0;$i<sizeof($array);$i++) $string.= $array[$i];
        return $string;
    }


    function uploadSettings($settings) {
        $query = array("table" => 'settings', "values" => "`value` = '$settings'", "where" => "`u_id` = '$this->U_ID'" );
        $result = $this->DB->UPDATE($query);
        return $result;
    }

    function getCurrentSettings() {
       $query = array("table" => 'settings', "wts" => "value", "where" => "`u_id` = '$this->U_ID'" );
       $result = $this->DB->SELECT($query);
       $res = $this->DB->pushToArray($result);
       return $res['value'];
   }

   function parse($str) {
        $this->settings = parent::parse($str);
   }

   function printout() {
        var_dump($this->settings);
   }


   function CreateSettingsList() {
        $this->ready_settings = array();
        for($i=0;$i<sizeof($this->setting_keys);$i++) $this->ready_settings[$this->setting_keys[$i]] = $this->settings[$i]*1;
        return $this->ready_settings;
   }


   function  getConfig($name) {
       $str = $this->getCurrentSettings();
       $this->parse($str);
       $settings_array =  $this->createSettingsList();
       return $settings_array[$name];
   }



}

