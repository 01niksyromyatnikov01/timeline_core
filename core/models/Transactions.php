<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 08.02.2018
 * Time: 14:50
 */

namespace models\Transactions;

function __autoload($class_name) {
    require_once MAIN_DIR."/core/models/".$class_name.".php";
}


class Transactions
{
    public $PUB_KEY,$message,$PUB_ACC;
    protected $DB,$PRIV_KEY,$PUB;


    function __construct($u_id,$fast_pay = false)
    {
        __autoload('Database');
        __autoload('Users');

        $this->DB = new \models\Database\Database();

         if(!$fast_pay) {
             $this->PUB = $this->getPUB_KEY($u_id);
             $this->PUB_KEY = $this->PUB['pub_key'];
             $this->PUB_ACC = $this->PUB['user_key'];
         }
    }



    private function getPUB_KEY($u_id)
    {
        $query = array("table" => "connects", "wts" => "`pub_key`,`user_key`", "where" => "`id` = '$u_id' ");
        $result = $this->DB->SELECT($query);
        $res_row = $this->DB->pushToArray($result);
        return ['pub_key' => $res_row['pub_key'],'user_key' => $res_row['user_key']];
    }




    function Create($priv1,$pub2,$value) {
        $this->PRIV_KEY = $priv1;
        $message = $this->PUB_KEY.$this->PRIV_KEY.$value;
        $this->message = hash('sha256',$message).$pub2;
        if($this->message != '') $this->Send($value,$pub2);
    }

    private function Send($value,$pub2) {
        $value = htmlspecialchars($value);
        $query = array("table" => "transact", "cells" => "(`message`,`value`,`reciever`)", "values" => "('$this->message','$value','$pub2')");
        $result = $this->DB->INSERT($query);
        $trans_id = $this->DB->getLastId();
        if(!$result) $this->Create($this->PRIV_KEY,$pub2,$value);
        $result = $this->MinusBalance($value);
        if($result) $result = $this->PlusBalance($value,$pub2);
        $result = $this->UnblockBill();
        $this->Clean();

        // Actions Inserting
        $act['type'] = 'sent';
        $act['trans_id'] = $trans_id;
        $act['coins'] = $value;
        $User = new \Actions;
        $User->UpdateAction($act);
        // End
        return $result;
    }


    private function Clean() {
        $_SESSION['pub2'] = '';
        $_SESSION['priv1'] = '';
        $_SESSION['cash'] = '';
    }

    private function MinusBalance($value) {
        $query = array("table" => 'bills',"values" => "`active` = `active` - $value", 'where'=>"`pub_key` = '$this->PUB_KEY'");
        $result = $this->DB->UPDATE($query);
        if($result) {
            $query = array("table" => 'bills',"values" => "`blocked_out` = `blocked_out` + $value", 'where'=>"`pub_key` = '$this->PUB_KEY'");
            $result = $this->DB->UPDATE($query);
            }
        return $result;
    }

    private function PlusBalance($value,$pub2) {
        $value -= $value*0.01;
        $query = array("table" => 'bills',"values" => "`blocked_in` = `blocked_in` + $value", 'where'=>"`pub_key` = '$pub2'");
        $result = $this->DB->UPDATE($query);
        return $result;
    }


    function CheckBills() {
        $query = array("table" => "bills", "wts" => "`active`,`blocked_out`,`card_limit`", "where" => "`pub_key` = '$this->PUB_KEY' ");
        $result = $this->DB->SELECT($query);
        $res_row = $this->DB->pushToArray($result);
        return $res_row;
    }

    function BlockBill() {
        $query = array("table" => 'bills',"values" => '`status` = "blocked"', 'where'=>"`pub_key` = '$this->PUB_KEY'");
        $result = $this->DB->UPDATE($query);
        return $result;
    }

    function UnblockBill() {
        $query = array("table" => 'bills',"values" => '`status` = "active"', 'where'=>"`pub_key` = '$this->PUB_KEY'");
        $result = $this->DB->UPDATE($query);
        return $result;
    }

}


class UserSign extends Transactions
{
    private $user,$pub_key;
    function __construct($u_id)
    {
        parent::__construct($u_id);
        __autoload("Files");
    }

    function getSignature($filename)
    {
        $ReadFile = new \models\Files\ReadFile;
        $this->user = $ReadFile->ReadFile($filename);
        $points = $this->getStartEndParams();
        $this->pub_key = hash('sha256',substr($this->user['hash'],$points['start'],$points['finish']));
        if($this->pub_key === $this->PUB_KEY) {
            return $this->user['hash'];
        }
        else return '';
    }

    function getStartEndParams() {
        $array = [];
        $id = htmlspecialchars($_SESSION['id']);
        if($id%2 != 0) {
            $array['start'] =$id%2 + 5;
            $array['finish'] = 32;
        }
        else {
            $array['start'] = 5;
            $array['finish'] = 32;
        }
        return $array;
    }
}



class FastPay extends Transactions {
    private $pincode;
    public $PUB2,$ID;

   function  __construct($data) {
       parent::__construct(0,true);
       $this->PUB = $this->getPUB_KEY_BY_PIN($data['pincode']);
       $this->PUB_KEY = $this->PUB['pub_key'];
       $this->PUB_ACC = $this->PUB['user_key'];
       $this->PUB = $this->getPUB_KEY_BY_OWNER_ID($data['owner_id']);
       $this->PUB2 = $this->PUB['pub_key'];
       $this->ID = $this->getID_BY_PUB_KEY();
   }

private function CheckIfBanned($ip) {
       $query = array("table" => 'ban',"wts" => '`tried`', "where" => "`ip` = '$ip' ");
       $result = $this->DB->SELECT($query);
       $res = $this->DB->pushToArray($result);
       //var_dump($res['tried']);
       return $res['tried'];
}

private function Ban() {
       require_once MAIN_DIR.'/core/func/IP.php';
       $ip = getRealIP();
       $tried = $this->CheckIfBanned($ip);
       if($tried == Null) {
           $query = array("table" => 'ban', "cells" => '(`ip`,`ban_time`)', "values" => "('$ip','40000')");
           $res = $this->DB->INSERT($query);
       }
       else {
           $query = array("table" => 'ban', "values" => "`tried` = `tried` + 1, `ban_time` = `ban_time` + 40000", "where" => "`ip` = '$ip'");
           $res = $this->DB->UPDATE($query);
           if($tried+1 > 3) $res =  'banned';
       }
       return $res;
}



private function getPUB_KEY_BY_PIN($pin)
{
    $this->pincode = hash('sha256',$pin);
    $query = array("table" => "bills", "wts" => "`pub_key`", "where" => "`id` = (SELECT `bill_id` FROM `pay_campaigns` WHERE `pin` = '$this->pincode' AND `status` = 'active')");
    $result = $this->DB->SELECT($query);
    $res_row = $this->DB->pushToArray($result);
    return ['pub_key' => $res_row['pub_key'],'user_key' => ''];
}

private function getPUB_KEY_BY_Owner_ID($owner_id)
{
    $query = array("table" => "bills", "wts" => "`pub_key`", "where" => "`id` = (SELECT `bill_id` FROM `pay_campaigns` WHERE `hash_id` = '$owner_id')");
    $result = $this->DB->SELECT($query);
    $res_row = $this->DB->pushToArray($result);
    return ['pub_key' => $res_row['pub_key'],'user_key' => ''];
}
private function getID_BY_PUB_KEY()
{
    if(empty($this->PUB_KEY)) {
        $res = $this->Ban();
        if($res === 'banned') return $res;
    }
    $query = array("table" => "connects", "wts" => "`id`", "where" => "`pub_key` = '$this->PUB_KEY'");
    $result = $this->DB->SELECT($query);
    $res_row = $this->DB->pushToArray($result);
    return $res_row['id'];
}



}