<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 05.01.2018
 * Time: 14:20
 */

namespace models\Clusters;


class Clusters
{
   private $DB;

   function __construct()
   {
       require_once MAIN_DIR.'/core/models/Database.php';
       $this->DB = new \models\Database\Database();
   }

   function Check($rec_id) {
       $id =  $_SESSION['id'];
       $query = array('table'=>'notifications','wts'=>'id','where'=>'sender_id='.$id.' AND reciever_id='.$rec_id.' ');
       $result = $this->DB->SELECT($query);
       $res = $this->DB->pushToArray($result);
       return $res['id'];
   }

   function Create() {
          $id = $_SESSION['id'];
          $query = array('table' => 'clusters','cells'=>'(`users_id`,`owner_id`)','values'=>'('.$id.','.$id.')');  //'users_id'=>''.$id.'', 'owner_id' => '' . $id . '');
          $ins = $this->DB->INSERT($query);
          $query = array('table'=>'clusters','wts'=>'`id`','where'=>'`owner_id` = '.$id.' ORDER BY `id` DESC');
          $select = $this->DB->SELECT($query);
          $res = $this->DB->pushToArray($select);
          return $res['id'];
   }

   function ComeIn($clr_id) {
      $id = ','.$_SESSION['id'];
      $query = array('table'=>'clusters','values'=>'`users_id`=CONCAT(users_id,"'.$id.'") ','where'=>'`id`='.$clr_id.'');
      $res = $this->DB->UPDATE($query);
      return $res;
   }

   function SendInv($rec_id) {
       $sender_id = $_SESSION['id'];
       if($this->CheckSettings($rec_id,"invites_cluster")) {
           $check = $this->Check($rec_id);
           if (empty($check)) {
               $clr_id = $this->Create();
               $time = time();
               $message = '<label>User #' . $sender_id . ' invites You to join cluster</label><button id=""  class="btn btn-outline-success btn-sm" onclick=JoinCluster(' . $clr_id . ') type=submit>Accept</button>';
               $message = htmlspecialchars($message);
               $query = array('table' => 'notifications', 'cells' => '(`sender_id`,`reciever_id`,`message`,`type`,`time`)', 'values' => '(' . $sender_id . ',' . $rec_id . ',"' . $message . '","1","' . $time . '")');
               $ins = $this->DB->INSERT($query);
           }
           return $ins;
       }
       else return false;
   }

   function CheckSettings($id,$param) {
       require_once MAIN_DIR.'/core/models/Settings.php';
       $Settings = new \models\Settings\UserSettings($id);
       return  $Settings->getConfig($param);
   }
}


