<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 21.02.2018
 * Time: 23:07
 */
namespace  func\inCluster;

require_once MAIN_DIR.'/core/models/Database.php';


function Check()
{
    if(isset($_SESSION['id'])) {
        $id = htmlspecialchars($_SESSION['id']);
        $DB = new \models\Database\Database();
        $query = array('table' => 'clusters', 'wts' => 'id', 'where' => "`users_id` LIKE '%$id%'");
        $result = $DB->SELECT($query);
        $row = $DB->pushToArray($result);
        if(isset($row['id'])) return $row['id'];
        else return false;
    }
    return false;
}