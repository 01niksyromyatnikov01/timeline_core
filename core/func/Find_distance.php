<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 02.01.2018
 * Time: 22:10
 */
namespace func\Distance;
  function getDistance($lat1, $lon1, $lat2, $lon2) {
    $lat1 *= M_PI / 180;
    $lat2 *= M_PI / 180;
    $lon1 *= M_PI / 180;
    $lon2 *= M_PI / 180;

    $d_lon = $lon1 - $lon2;

    $slat1 = sin($lat1);
    $slat2 = sin($lat2);
    $clat1 = cos($lat1);
    $clat2 = cos($lat2);
    $sdelt = sin($d_lon);
    $cdelt = cos($d_lon);

    $y = pow($clat2 * $sdelt, 2) + pow($clat1 * $slat2 - $slat1 * $clat2 * $cdelt, 2);
    $x = $slat1 * $slat2 + $clat1 * $clat2 * $cdelt;

    return round((atan2(sqrt($y), $x) * 6372795)/1000,2);
}


//echo getDistance('46.28','30.43','59.924526','30.349196');