<?php

namespace  func\UserActions;
require_once MAIN_DIR.'/core/models/Database.php';


function Generate($type) {
    $DB = new \models\Database\Database();

    $where = getType($type);

    $print = '';
    $where .= "AND `u_id` = $_SESSION[id]";
    $query = array('table'=>'user_actions','wts'=>'act,trans_id,coins,time','where'=>"$where ORDER BY id DESC");
    $result = $DB->SELECT($query);


    $i=1;
    while($row = mysqli_fetch_assoc($result)) {
        $commission = CalcCommission($row['coins']);
        $print .= '<td>'.$i.'</td><td>'.$row['time'].'</td><td>'.$row['trans_id'].'</td><td>'.$row['act'].'</td><td>'.$row['coins'].' TCN</td><td>'.$commission.' TCN</td></tr>';
        $i++;
    }
    return $print;
}


function getType($type) {
    $where = '';
    if($type == 'transactions') $where = "(`act` = 'sent' OR `act` = 'recieved')";
    return $where;
}


function CalcCommission($value) {
    return $value - round((($value*100)/101), 2);
}