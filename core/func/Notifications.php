<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 07.01.2018
 * Time: 23:43
 */

namespace func\Notifications;



function Show($action) {
    include MAIN_DIR.'/core/models/Database.php';
    $print = '';
    $id = $_SESSION['id'];

    $DB = new \models\Database\Database();
    $query = array('table'=>'notifications','wts'=>'*','where'=>'`reciever_id`='.$id.' AND `seen`=0 ORDER BY ID DESC');
    $result = $DB->SELECT($query);

    $i = 0;

    while($row = mysqli_fetch_assoc($result)) {
        if($i==0) $i++;
        $print .= '<div class="notifications" id="not_num_'.$row['id'].'">'.htmlspecialchars_decode($row['message']).'<i  class="fa fa-times" onclick="DeleteNot('.$row['id'].')" aria-hidden="true"></i></div>';
        $i++;
    }


    if($i>0 AND $action == 'clean') {
        $query = array('table'=>'notifications','values'=>'`seen`=1 ','where'=>'`reciever_id`='.$id.' AND `seen`=0');
        $update = $DB->UPDATE($query);
    }
    $result = array('content'=>$print,'count'=>$i);

    return $result;
}


function Delete($not_id) {
    include MAIN_DIR.'/core/models/Database.php';

    $id = $_SESSION['id'];

    $DB = new \models\Database\Database();

    $query = array('table'=>'notifications','where'=>'`reciever_id`='.$id.' AND `id`='.$not_id.'');
    $result = $DB->DELETE($query);

    return $result;
}

function JoinClr($clr_id) {
    $clr_id = htmlspecialchars($clr_id);
    require_once MAIN_DIR.'/core/models/Clusters.php';
    $Cluster = new \models\Clusters\Clusters();
    $result = $Cluster->ComeIn($clr_id);

    return $result;
}



function getInvitesList($u_id) {
    $Database = new \models\Database\Database();

    $u_id = htmlspecialchars($u_id);
    $query = array('table'=>'notifications','wts'=>'`reciever_id`','where'=>'`sender_id`='.$u_id.'');
    $select = $Database->SELECT($query);
    $result = array();
    while($row = mysqli_fetch_assoc($select)) {
        $result[] = $row['reciever_id'];
    }
    return $result;
}