<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 04.01.2018
 * Time: 23:21
 */

namespace  func\ClusterTableGen;
require_once MAIN_DIR.'/core/models/Database.php';
require_once MAIN_DIR.'/core/func/Find_distance.php';
require_once MAIN_DIR.'/core/func/Notifications.php';

function Generate() {
    $DB = new \models\Database\Database();

    $print = '';
    $query = array('table'=>'location','wts'=>'lat,lng','where'=>'`u_id` = '.$_SESSION['id'].'');
    $result = $DB->SELECT($query);
    $u_coords = $DB->pushToArray($result);
    $query = array('table'=>'location','wts'=>'*','where'=>'`u_id` !='.$_SESSION['id'].'');
    $result = $DB->SELECT($query);

    //Проверка на наличие отправленного инвайта
    $rec_array = \func\Notifications\getInvitesList($_SESSION['id']);

    $i=1;
    while($row = mysqli_fetch_assoc($result)) {

        if(in_array($row['u_id'],$rec_array)) {
            $button = '<button  class="btn btn-outline-primary btn-sm" disabled  type="submit" >Sent</button>';
        }
        else {$button = '<button  class="btn btn-outline-success btn-sm" onclick="SendInv('.$row['u_id'].')" type="submit">Connect</button>';}

    $dist = GetUserLoc($u_coords['lat'],$u_coords['lng'],$row['lat'],$row['lng']);
    $print .= '<td>'.$i.'<td>'.$row['u_id'].'</td><td>'.$dist.' km</td><td>'.substr($row['time'],0,19).'</td><td></td><td id="conn_button_'.$row['u_id'].'">'.$button.'</td></tr>';
    $i++;
    }
return $print;
}




function GetUserLoc($lat1,$lon1,$lat2,$lon2) {
    $distance = \func\Distance\getDistance($lat1,$lon1,$lat2,$lon2);
    return $distance;
}