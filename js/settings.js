$(document).ready(function() {
    var data = {};
    data['url'] = 'get';
    SenderReciever(data);

})

var ru = {};
ru['turn_on'] = 'Включить';
ru['turn_off'] = 'Выключить';
ru['success_change'] = 'Успешно изменено';


function GetCurSettings(answer) {
    if(!answer['error']) {
        PrintCurSettings(answer['settings']);
    }
}

function PrintCurSettings(data) {
    for(var setting in data) {
        if (data[setting] == 0) text = '<button class="btn btn-outline-success" onclick=SetNewSettings(1,"' + setting + '")>' + ru['turn_on'] + '</button>';
        else text = '<button class="btn btn-outline-danger" onclick=SetNewSettings(0,"' + setting + '")>' + ru['turn_off'] + '</button>';
        document.getElementById(setting).innerHTML = text;
    }
}


function SetNewSettings(value,field) {
    var data = {};
    data['url'] = 'set';
    data['value'] = value;
    data['field'] = field
    var answer = SenderReciever(data)
}

function number_validate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
    else {}
}


function setSendLimit() {
   var input =  document.getElementById('max_send_limit');
   var data = {};
    data['value'] = parseFloat(input.value);
   if( data['value'] > 0 ) {
       data['field'] = 'card_limit';
       data['url'] = 'set';
      var answer =  SenderReciever(data);
   }
}



function EndTimeClick() {
    var data = {};
    data['url'] = 'delete';
    data['field'] = 'delete_button_settings';
    var answer =  SenderReciever(data);
}


function SenderReciever(info) {
    $.ajax({ // инициaлизируeм ajax зaпрoс
        type: 'POST', // oтпрaвляeм в POST фoрмaтe, мoжнo GET
        url: '/user/settings/'+info['url'],
        dataType: 'json', // oтвeт ждeм в json фoрмaтe
        data: info, // дaнныe для oтпрaвки
        beforeSend: function (data) {
            if(info['url'] === 'set')  document.getElementById(info['field']).disabled = true;
        },
        success: function (data) {
            if (data['error']) { // eсли oбрaбoтчик вeрнул oшибку

            }
            else { // eсли всe прoшлo oк
                if(info['url'] === 'get') GetCurSettings(data);
                else if(info['url'] === 'set' && info['field'] != 'card_limit') {
                    if (info['value'] == 0) text = '<button class="btn btn-outline-success" onclick=SetNewSettings(1,"' + info['field'] + '")>' + ru['turn_on'] + '</button>';
                    else text = '<button class="btn btn-outline-danger" onclick=SetNewSettings(0,"' + info['field'] + '")>' + ru['turn_off'] + '</button>';
                    document.getElementById(info['field']).innerHTML = text;
                }
                else if(info['field'] === 'card_limit') ShowAlert();
                else if(info['url'] === 'delete') document.location.href = '../../';
            }
        },
        error: function (jqXHR) {
            if (jqXHR.status === 500) {
                document.location.href = '';
            }
        },
        complete: function (data) { // сoбытиe пoслe любoгo исхoдa
            if(info['url'] === 'set')
            document.getElementById(info['field']).disabled = false;
        }

    });

}



function ShowAlert() {
    var div = document.createElement('div');
    div.id = 'alert_block';
    div.innerHTML = '<div class="alert alert-success" role="alert">'+ru['success_change']+'</div>'
    document.body.appendChild(div);
    $('#alert_block').show('slow');
    setTimeout(function() { $('#alert_block').hide('slow');
    }, 4000);
}