importScripts('../js/encode.js');
self.addEventListener('message', function(e) {
    const K = Math.random();
    var start,time,stop,nounce = 1;
    start = (new Date()).getTime();
    var data = e.data;
    for (var i = 1; i > 0; i++) {
        nounce = nounce+(K+1)*2;
        nounce.toFixed(8);
        var str = encode(encode(data.text + nounce));
        if (str.substring(0, 5) === '00000') {
            stop = (new Date()).getTime();
            break;
        }
    }
    time = stop - start;
    self.postMessage({'result':str,'nounce':nounce,'time':time});
    self.close(); // Terminates the worker.
},false);