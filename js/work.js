const diff = 5;
var workers_num = 1;
var worker = {};
var Working = false;
var MiningType = 'solo';
var cluster_id = 0;
var CheckwithInterval;


function ChangeMiningType(value) {
    var MiningType = value;
    if(value != 0) {cluster_id = value; document.getElementById('mining_types_cluster').style.color = '#000';document.getElementById('mining_types_solo').style.color = '#121212ab';}
    else {cluster_id = 0;document.getElementById('mining_types_solo').style.color = '#000'; document.getElementById('mining_types_cluster').style.color = '#121212ab'};

}


function GetDifficulty() {
    var value = '';
    var input_value = document.getElementById("diff_input").value;
    if(input_value < 3) {
        value = 'Minimal';
    }
    if(input_value < 5 && input_value > 2) {
        value = 'Hard';
    }
    if(input_value > 4) {
        value = 'For monsters'
    }
    document.getElementById("show_difficulty").innerHTML = '<b>'+input_value+'</b>: '+value;
    workers_num = input_value;
}


function Workers(data,workers_num) {
    for(var i=0;i<workers_num;i++) {
        worker[i] = new Worker('../js/worker.js');
        worker[i].addEventListener('message', function(e) {
            for(i=0;i<workers_num;i++) {worker[i].terminate();}
            UpdateTask(e.data.result,e.data.nounce,e.data.time,data['id'],data['reciever']);
        }, false);

        worker[i].postMessage({'text':data['text']});
    }
}

function StartWork() {
    if(!Working) GetTask();
    else Stop();
}


function Stop() {
    for(var i=0;i<workers_num;i++) {worker[i].terminate();}
    Working = false;
    document.getElementById("start_text_in_circle").innerText = 'Start';
    document.getElementById("start_img_in_circle").style.display = "none";
    document.getElementById("working_text_in_circle").style.display = "none";
    document.getElementById("start_text_in_circle").style.display = "block";
    document.getElementById("difficulty_block").style.display = "block";
}


function GetTask() {
    var data = ''; // пoдгoтaвливaeм дaнныe
    var url = '';
    if(cluster_id != 0) url = 'task/cluster/'+cluster_id;
    else url = 'tasks';
    $.ajax({ // инициaлизируeм ajax зaпрoс
        type: 'GET', // oтпрaвляeм в POST фoрмaтe, мoжнo GET
        url: 'http://localhost:3030/api/v1/'+url,
        dataType: 'json', // oтвeт ждeм в json фoрмaтe
        data: data, // дaнныe для oтпрaвки
        beforeSend: function(data) {
        document.getElementById("start_img_in_circle").style.display = "block";
        document.getElementById("working_text_in_circle").style.display = "block";
        document.getElementById("start_text_in_circle").style.display = "none";
        document.getElementById("difficulty_block").style.display = "none";
        },
        success: function(data){
            if (data['error']) { // eсли oбрaбoтчик вeрнул oшибку
                //alert(data['error']);
                document.getElementById("working_text_in_circle").innerText = 'No tasks available';
                setInterval( function() {
                    document.getElementById("start_text_in_circle").innerText = 'Start';
                    document.getElementById("start_img_in_circle").style.display = "none";
                    document.getElementById("working_text_in_circle").style.display = "none";
                    document.getElementById("start_text_in_circle").style.display = "block";
                    document.getElementById("difficulty_block").style.display = "block";

                },7000)
            }
            else { // eсли всe прoшлo oк
                Working = true;
                CheckwithInterval=setInterval(CheckTaskAccessibility,30000,data['id']);
                Workers(data,workers_num);
            }
        },
        complete: function(data) { // сoбытиe пoслe любoгo исхoдa

        }

    });
}

function CountDiff(time) {
    var Count = ((time/3600000)*5).toFixed(2);
    return Count;
}

function CountTime(time) {
        var d = new Date(time),
        hours = d.getHours(),
        minutes = d.getMinutes(),
        seconds = d.getSeconds();
        if(time <= 3600*1000) hours = 0;
        var arr_time  = {'hours':hours,'minutes':minutes,'seconds':seconds};
        return arr_time;
}

function UpdateTask(result,nounce,time,task_id,reciever) {
    var solver = Number(getCookie('u_id'));
    var data = JSON.stringify({result:result,nounce:nounce,solver:solver,reciever:reciever,cluster_id:cluster_id});
    $.ajax({ // инициaлизируeм ajax зaпрoс
        type: 'POST', // oтпрaвляeм в POST фoрмaтe, мoжнo GET
        url: 'http://localhost:3030/api/v1/tasks/'+task_id,
        dataType: 'json', // oтвeт ждeм в json фoрмaтe
        data: data, // дaнныe для oтпрaвки
        beforeSend: function(data) {
        },
        success: function(data){
            if (data['error']) { // eсли oбрaбoтчик вeрнул oшибку
            }
            else { // eсли всe прoшлo oк
                var time_arr = CountTime(time);
                var table = document.getElementById('tbody_mining');
                var row = document.createElement('tr');
                row.innerHTML = '<td>'+task_id+'</td><td>'+nounce+'</td><td>'+CountDiff(time)+'</td><td></td><td>'+time_arr['hours']+'h '+time_arr['minutes']+'m '+time_arr['seconds']+'s</td>';
                table.appendChild(row);
                GetTask();

            }
        },
        error: function (jqXHR) {
            if (jqXHR.status === 500) {
               UpdateTask(result,nounce,time,task_id);
            }
        },
        complete: function(data) { // сoбытиe пoслe любoгo исхoдa

        }
    });
}


function CheckTaskAccessibility(task_id) {
    var data = '';
   $.ajax({ // инициaлизируeм ajax зaпрoс
        type: 'POST', // oтпрaвляeм в POST фoрмaтe, мoжнo GET
        url: 'http://localhost:3030/api/v1/access/task/'+task_id,
        dataType: 'json', // oтвeт ждeм в json фoрмaтe
        data: data, // дaнныe для oтпрaвки
        beforeSend: function(data) {
        },
        success: function(data){
            if (data['error']) { // eсли oбрaбoтчик вeрнул oшибку
            }
            else { // eсли всe прoшлo oк
             if(data['result'] === 'finished') {Stop();StartWork();clearInterval(CheckwithInterval);}
            }
        },
        error: function (jqXHR) {
            if (jqXHR.status === 500) {
                CheckTaskAccessibility(task_id);
            }
        },
        complete: function(data) { // сoбытиe пoслe любoгo исхoдa
        }
    });
}



// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}