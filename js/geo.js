

// Check if the browser has support for the Geolocation API
if (!navigator.geolocation) {

    window.location = '../../location/ip';

} else {

     function SetCoords() {


        navigator.geolocation.getCurrentPosition(function(position) {

            // Get the coordinates of the current possition.
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;

            //alert(lat.toFixed(3));
            //alert(lng.toFixed(3));

            var date = new Date(new Date().getTime() + 60 * 1000);

            document.cookie = "lat="+lat+"; path=/; expires=" + date.toUTCString()
            document.cookie = "lng="+lng+"; path=/; expires=" + date.toUTCString()
            window.location = '../../user/setCoords/';

        });

    };

     SetCoords();
}

