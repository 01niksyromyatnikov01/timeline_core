

function ChooseFile() {
    document.getElementById('key_upload_input').click();
}

function UploadFile() {
    //alert(document.getElementById('key_upload_input').value);
    //if(document.getElementById('key_upload_input').value != '') {}
    document.getElementById('submit_button_form').click();
}

function Menu() {
    var menu = document.getElementById('mob_menu')
    var status = menu.style.display;
    if(status == 'block')  menu.style.display = 'none';
    else menu.style.display = 'block';
}


function Show(id) {
    icon = document.getElementById(id+'_icon')
    var div = document.getElementById(id);
    var display = div.style.display;
    if(display == 'block') {
        div.style.display = 'none';
        icon.className = 'right fas fa-plus';
    }
    else {
        div.style.display = 'block';
        icon.className = 'right fas fa-minus';
    }
}


function Alerts(color,info) {
    var div = document.createElement('div');
    div.id = 'alert_block';
    div.innerHTML = '<div class="alert alert-'+color+'" role="alert">'+info+'</div>'
    document.body.appendChild(div);
    $('#alert_block').show('slow');
}