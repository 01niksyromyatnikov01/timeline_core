<?php

$common = [
    'start' => 'Start',
    'specials' => 'Особенности',
    'login' => 'Login',
    'api' => 'API',
    'auth' => 'Authorization',
    'download_file_key' => 'Download the key file to log into the system ',
    'download_key' => 'Download key',
    'reciever' => 'Reciever',
    'sender' => 'Sender',
    'pay_with_com' => 'Payment(Commission is included)',
    'pay' => 'Pay',
    'last_transactions' => 'Last transactions',
    'all_human_time' => 'All people time',
    'surrender_to_time' => 'Surrender to the power of Time!',
    'join_text' => 'Join the revolutionary currency system, developed on the basis of the latest technologies in the field of crypto currency. ',
    'learn_more' => 'Learn more',
    'key_was_generated_for_you' => 'A key-file was generated for you!',
    'dont_send_it' => 'Do not send it or lose it',
    'this_file_is_important' => 'This file will be required for your login and access to your wallet! In the event of a lost key, you will not be able to recover your account.',
];


$user_loc = [
    'account' => 'Account',
    'settings' => 'Settings',
    'help' => 'Help',
    'logout' => 'Logout',
    'main' => 'Home',
    'transactions' => 'Transactions',
    'drop' => 'Mining',
    'clusters' => 'Clusters',
    'actions' => 'Actions',
    'send' => 'Transfer',
    'recieve' => 'Recieve',
    'connect' => 'Unite',
    'wallet' => 'Wallet',
    'text_under_circle_1' => 'Almost instant transfers within the system',
    'text_under_circle_2' => 'Use your device to get TimeCoin',
    'text_under_circle_3' => 'Create a cluster or join an existing one',
    'text_under_circle_4' => 'Your personal TimeCoin bill',
    'newest_trans_blocks' => 'The newest transaction blocks',
    'active_users' => 'Active users',
];





//transactions

$transactions = [
    'transactions' => 'Transfers',
    'send' => 'Transfer coins',
    'my_address' => 'Your PUB-address',
    'reciever' => 'Reciever',
    'reciever_address' => 'Reciever PUB-address',
    'quantity' => 'Transfer amount',
    'balance' => 'Balance',
    'commission' => 'Commission',
    'send_button' => 'Transfer',
    'confirmation' => 'Confirmation of transaction',
    'confirm info' => 'All payments in the Timeline system must be signed by the sender before sending. Only so we can make sure that this is really you. To do this, download the key file that is used when logging in.',
    'confirm_button' => 'Upload',
    'signed_succesfully' => 'Successfully signed'
]


?>