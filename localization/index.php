<?php
/**
 * Created by PhpStorm.
 * User: niksy
 * Date: 14.02.2018
 * Time: 18:43
 */

if( $_COOKIE['lang'] == 'ru' ) {
    $file = 'ru';
}
else if( $_COOKIE['lang'] == 'en' ) {
    $file = 'en';
}
else {
    $file = 'ru';
}

require_once($file.'.php');